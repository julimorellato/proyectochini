package com.chini.analisisgranos

class Client {

	String name
	String cuit
	Address address
	
    static constraints = {
		name nullable:false, maxSize:5
    }
}
